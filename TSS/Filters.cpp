#include "Filters.h"



Filters::Filters(QString type, QImage original, QString color)
{
	originalImage = original;
	filter = type;
	ditherColor = color;
}


Filters::~Filters()
{
}

void Filters::createPalette(qint32 paletteSize)
{

	qint32 width = originalImage.width(), height = originalImage.height(), n = width * height;
	QColorVector colors(n);
	for (qint32 y = 0, i = 0; y < height; y++) {
		for (qint32 x = 0; x < width; x++, i++)
			colors[i] = originalImage.pixel(x, y);
	}
	auto last = std::unique(colors.begin(), colors.end());
	colors.erase(last, colors.end());

	splitBin(colors.begin(), colors.end(), paletteSize, paletteSize);
}

void Filters::clearPalette()
{
	palette.clear();
}

void Filters::colorReduce()
{
	QImage::Format format = originalImage.format();
	int height = originalImage.height();
	int width = originalImage.width();

	imageOldFilm = QImage(width, height, format);
	
	for (qint32 y = 0; y < height; y++) {
		for (qint32 x = 0; x < width; x++) {
			imageOldFilm.setPixelColor(x, y, colorFromPalette(originalImage.pixel(x, y)));
		}
	}
	emit filterComputationComplete();
}

QString Filters::getType()
{
	return filter;
}

QImage Filters::getDitherFSImage()
{
	return imageDitherFS;
}

QImage Filters::getDitherBasicImage()
{
	return imageDitherBasic;
}

QImage Filters::getOldFilmImage()
{
	return imageOldFilm;
}

QImage Filters::getOriginalImage()
{
	return originalImage;
}

void Filters::ditherBasic()
{
	QImage::Format format = originalImage.format();
	int height = originalImage.height();
	int width = originalImage.width();

	imageDitherBasic = QImage(width, height, format);

	int randomR,randomG,randomB;
	QColor colorOld, colorNew;
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			randomR = rand() % 256;
			randomG = rand() % 256;
			randomB = rand() % 256;
			colorOld = originalImage.pixelColor(x, y);
			
			if (colorOld.red() + colorOld.green() + colorOld.blue() > randomR + randomB + randomG)
				colorNew.setNamedColor(ditherColor);
			else
				colorNew = colorOld;
			imageDitherBasic.setPixelColor(x,y,colorNew);
		}
	}
	emit filterComputationComplete();
}

QImage Filters::ditherBlackWhite()
{
	QImage::Format format = originalImage.format();
	int height = originalImage.height();
	int width = originalImage.width();

	imageDitherBasic = QImage(width, height, format);

	int randomR, randomG, randomB;
	QColor colorOld, colorNew;
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			randomR = rand() % 256;
			randomG = rand() % 256;
			randomB = rand() % 256;
			colorOld = originalImage.pixelColor(x, y);

			if (colorOld.red() + colorOld.green() + colorOld.blue() > randomR + randomG + randomB)
				colorNew.setNamedColor("white");
			else
				colorNew.setNamedColor("black");
			imageDitherBasic.setPixelColor(x, y, colorNew);
		}
	}
	return imageDitherBasic;
}

QColor Filters::colorFromPalette(QRgb color)
{
	auto colorDistance = [](const QColor &a, QRgb b) -> qreal {
		return QVector3D(a.red() - qRed(b), a.green() - qGreen(b), a.blue() - qBlue(b)).length();
	};
	qint32 index = 0;
	qreal minDistance = colorDistance(palette[0], color);

	for (qint32 i = 1, size = palette.size(); i < size; i++) {
		qreal distance = colorDistance(palette[i], color);
		if (distance < minDistance) {
			minDistance = distance;
			index = i;
		}
	};

	return palette[index];
}

void Filters::splitBin(QColorVector::Iterator start, QColorVector::Iterator end, qint32 remaining, const qint32 bins)
{
	enum {
		Red, Green, Blue
	} channel;
	qint32 size = 0;

	using namespace boost::accumulators;
	accumulator_set<qreal, features<tag::min, tag::max, tag::mean(immediate)>> red, green, blue;
	for (QColorVector::Iterator i = start; i != end; ++i, size++) {
		red(qRed(*i));
		green(qGreen(*i));
		blue(qBlue(*i));
	}

	//determine significant channel
	qint32 redRange = max(red) - min(red), greenRange = max(green) - min(green), blueRange = max(blue) - min(blue);
	if (redRange >= greenRange && redRange >= blueRange)
		channel = Red;
	else if (greenRange >= blueRange)
		channel = Green;
	else
		channel = Blue;

	//sort the samples
	std::sort(start, end, [channel](QRgb a, QRgb b) -> bool {
		switch (channel) {
			case Red:
				return qRed(a) < qRed(b);
			case Green:
				return qGreen(a) < qGreen(b);
			case Blue:
			default:
				return qBlue(a) < qBlue(b);
		}
	});

	//add the averaged color to the palette and stop the recursic
	if (remaining <= 1) {
		palette.append(QColor(mean(red), mean(green), mean(blue)));
		return;
	}

	QColorVector::Iterator midpoint = start + (size >> 1);
	splitBin(start, midpoint - 1, remaining >> 1, bins);
	splitBin(midpoint, end, remaining >> 1, bins);
}

void Filters::ditherFloydSteinberg()
{
	QImage::Format format = originalImage.format();
	int height = originalImage.height();
	int width = originalImage.width();

	imageDitherFS = QImage(width, height, format);
	imageDitherFS = originalImage;


	QColor colorOld, colorNew, color;
	int Rquant_error, Gquant_error, Bquant_error;
	for (int y = 0; y < height - 1; y++) {
		for (int x = 1; x < width - 1; x++) {
			colorOld = imageDitherFS.pixelColor(x, y);
			colorNew = colorFromPalette(qRgb(colorOld.red(), colorOld.green(), colorOld.blue()));
			imageDitherFS.setPixelColor(x, y, colorNew);
			Rquant_error = colorOld.red() - colorNew.red();
			Gquant_error = colorOld.green() - colorNew.green();
			Bquant_error = colorOld.blue() - colorNew.blue();

			color = imageDitherFS.pixelColor(x + 1, y);
			imageDitherFS.setPixelColor(x + 1, y, qRgb(round(color.red() + Rquant_error * (7 / 16.)), round(color.green() + Gquant_error * (7 / 16.)), round(color.blue() + Bquant_error * (7 / 16.))));

			color = imageDitherFS.pixelColor(x - 1, y + 1);
			imageDitherFS.setPixelColor(x - 1, y + 1, qRgb(round(color.red() + Rquant_error * (3 / 16.)), round(color.green() + Gquant_error * (3 / 16.)), round(color.blue() + Bquant_error * (3 / 16.))));

			color = imageDitherFS.pixelColor(x, y + 1);
			imageDitherFS.setPixelColor(x, y + 1, qRgb(round(color.red() + Rquant_error * (5 / 16.)), round(color.green() + Gquant_error * (5 / 16.)), round(color.blue() + Bquant_error * (5 / 16.))));

			color = imageDitherFS.pixelColor(x + 1, y + 1);
			imageDitherFS.setPixelColor(x + 1, y + 1, qRgb(round(color.red() + Rquant_error * (1 / 16.)), round(color.green() + Gquant_error * (1 / 16.)), round(color.blue() + Bquant_error * (1 / 16.))));
		}
	}

	emit filterComputationComplete();

	/*for each y from top to bottom
		for each x from left to right
			oldpixel : = pixel[x][y]
			newpixel : = find_closest_palette_color(oldpixel)
			pixel[x][y] : = newpixel
			quant_error : = oldpixel - newpixel
			pixel[x + 1][y] : = pixel[x + 1][y] + quant_error * 7 / 16
			pixel[x - 1][y + 1] : = pixel[x - 1][y + 1] + quant_error * 3 / 16
			pixel[x][y + 1] : = pixel[x][y + 1] + quant_error * 5 / 16
			pixel[x + 1][y + 1] : = pixel[x + 1][y + 1] + quant_error * 1 / 16

*/
}
