#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = scaleImage(painterWidth, painterHeight, loadedImage.size().width(), loadedImage.size().height());
	image = loadedImage.scaled(newSize);
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

QSize PaintWidget::scaleImage(int width, int height, int newWidth, int newHeight)
{
	float w, h, scale;
	float sizeFactor =  width / height;
	float newSizeFactor = newWidth / newHeight;
	// ak je sizeFactor > 1, tak je obrazok roztiahnuty na sirku
	// ak je sizeFactor < 1, tak je obrazok roztiahnuty na vysku

	if (sizeFactor < newSizeFactor) {			//novy obrazok je sirsi ako predosly
		scale = (float)width / newWidth;
		w = width;								//sirku zafixujem podla sirky okna
		h = newHeight * scale;					//vysku zmensim o tolko co sa zmensila sirka
	}
	else {										//novy obrazok je vyssi/rovnaky ako predosly
		scale = (float)height / newHeight;
		h = height;								//zafixujem vysku podla vysky okna
		w = newWidth * scale;					//sirku zmensim o tolko o kolko sa zmensila vyska
	}
	
	return QSize(round(w), round(h));
}

bool PaintWidget::newImage(int x,int y)
{
	QImage loadedImage(x, y, QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	//resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName, "png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

int PaintWidget::getHeight()
{
	return image.height();
}

int PaintWidget::getWidth()
{
	return image.width();
}

QImage PaintWidget::getImage()
{
	return image;
}

const uchar *PaintWidget::getImageBits()
{
	return image.constBits();
}

int PaintWidget::getByteCount()
{
	return image.byteCount();
}

void PaintWidget::setPainterWidgetSize(int x, int y)
{
	painterWidth = x;
	painterHeight = y;
}

void PaintWidget::changeImage(QImage newImage)
{
	image = newImage;
	update();
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}
