/********************************************************************************
** Form generated from reading UI file 'TSS.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TSS_H
#define UI_TSS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TSSClass
{
public:
    QAction *actionOpen;
    QAction *actionClose;
    QAction *actionExit;
    QAction *actionDitherFloydSteinberg;
    QAction *actionOld_film;
    QAction *actionDitherRed;
    QAction *actionDitherBlue;
    QAction *actionDitherGreen;
    QAction *actionDitherCyan;
    QAction *actionDitherMagenta;
    QAction *actionDitherYellow;
    QAction *actionDitherBlackWhite;
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QGroupBox *groupBoxFileTree;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayoutFileTree;
    QListWidget *listWidget;
    QGroupBox *groupBoxHistogram;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayoutHistogram;
    QCheckBox *BlueCheckBox;
    QCheckBox *GreenCheckBox;
    QCheckBox *CurveDrawingCheckBox;
    QCheckBox *LockBitsCheckBox;
    QCheckBox *RedCheckBox;
    QGroupBox *groupBoxPicture;
    QGridLayout *gridLayout_5;
    QGridLayout *gridLayoutPicture;
    QMenuBar *menuBar;
    QMenu *menuMenu;
    QMenu *menuFilters;
    QMenu *menuDither_random;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *TSSClass)
    {
        if (TSSClass->objectName().isEmpty())
            TSSClass->setObjectName(QString::fromUtf8("TSSClass"));
        TSSClass->resize(872, 668);
        actionOpen = new QAction(TSSClass);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionClose = new QAction(TSSClass);
        actionClose->setObjectName(QString::fromUtf8("actionClose"));
        actionExit = new QAction(TSSClass);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionDitherFloydSteinberg = new QAction(TSSClass);
        actionDitherFloydSteinberg->setObjectName(QString::fromUtf8("actionDitherFloydSteinberg"));
        actionOld_film = new QAction(TSSClass);
        actionOld_film->setObjectName(QString::fromUtf8("actionOld_film"));
        actionDitherRed = new QAction(TSSClass);
        actionDitherRed->setObjectName(QString::fromUtf8("actionDitherRed"));
        actionDitherBlue = new QAction(TSSClass);
        actionDitherBlue->setObjectName(QString::fromUtf8("actionDitherBlue"));
        actionDitherGreen = new QAction(TSSClass);
        actionDitherGreen->setObjectName(QString::fromUtf8("actionDitherGreen"));
        actionDitherCyan = new QAction(TSSClass);
        actionDitherCyan->setObjectName(QString::fromUtf8("actionDitherCyan"));
        actionDitherMagenta = new QAction(TSSClass);
        actionDitherMagenta->setObjectName(QString::fromUtf8("actionDitherMagenta"));
        actionDitherYellow = new QAction(TSSClass);
        actionDitherYellow->setObjectName(QString::fromUtf8("actionDitherYellow"));
        actionDitherBlackWhite = new QAction(TSSClass);
        actionDitherBlackWhite->setObjectName(QString::fromUtf8("actionDitherBlackWhite"));
        centralWidget = new QWidget(TSSClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetMinAndMaxSize);
        groupBoxFileTree = new QGroupBox(centralWidget);
        groupBoxFileTree->setObjectName(QString::fromUtf8("groupBoxFileTree"));
        gridLayout_3 = new QGridLayout(groupBoxFileTree);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayoutFileTree = new QGridLayout();
        gridLayoutFileTree->setSpacing(6);
        gridLayoutFileTree->setObjectName(QString::fromUtf8("gridLayoutFileTree"));
        listWidget = new QListWidget(groupBoxFileTree);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));

        gridLayoutFileTree->addWidget(listWidget, 0, 0, 1, 1);


        gridLayout_3->addLayout(gridLayoutFileTree, 0, 0, 1, 1);


        gridLayout->addWidget(groupBoxFileTree, 0, 0, 1, 1);

        groupBoxHistogram = new QGroupBox(centralWidget);
        groupBoxHistogram->setObjectName(QString::fromUtf8("groupBoxHistogram"));
        gridLayout_4 = new QGridLayout(groupBoxHistogram);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayoutHistogram = new QGridLayout();
        gridLayoutHistogram->setSpacing(6);
        gridLayoutHistogram->setObjectName(QString::fromUtf8("gridLayoutHistogram"));
        BlueCheckBox = new QCheckBox(groupBoxHistogram);
        BlueCheckBox->setObjectName(QString::fromUtf8("BlueCheckBox"));

        gridLayoutHistogram->addWidget(BlueCheckBox, 2, 0, 1, 1);

        GreenCheckBox = new QCheckBox(groupBoxHistogram);
        GreenCheckBox->setObjectName(QString::fromUtf8("GreenCheckBox"));

        gridLayoutHistogram->addWidget(GreenCheckBox, 1, 0, 1, 1);

        CurveDrawingCheckBox = new QCheckBox(groupBoxHistogram);
        CurveDrawingCheckBox->setObjectName(QString::fromUtf8("CurveDrawingCheckBox"));

        gridLayoutHistogram->addWidget(CurveDrawingCheckBox, 4, 0, 1, 1);

        LockBitsCheckBox = new QCheckBox(groupBoxHistogram);
        LockBitsCheckBox->setObjectName(QString::fromUtf8("LockBitsCheckBox"));

        gridLayoutHistogram->addWidget(LockBitsCheckBox, 3, 0, 1, 1);

        RedCheckBox = new QCheckBox(groupBoxHistogram);
        RedCheckBox->setObjectName(QString::fromUtf8("RedCheckBox"));

        gridLayoutHistogram->addWidget(RedCheckBox, 0, 0, 1, 1);


        gridLayout_4->addLayout(gridLayoutHistogram, 0, 0, 1, 1);


        gridLayout->addWidget(groupBoxHistogram, 1, 0, 1, 1);

        groupBoxPicture = new QGroupBox(centralWidget);
        groupBoxPicture->setObjectName(QString::fromUtf8("groupBoxPicture"));
        gridLayout_5 = new QGridLayout(groupBoxPicture);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayoutPicture = new QGridLayout();
        gridLayoutPicture->setSpacing(6);
        gridLayoutPicture->setObjectName(QString::fromUtf8("gridLayoutPicture"));

        gridLayout_5->addLayout(gridLayoutPicture, 0, 0, 1, 1);


        gridLayout->addWidget(groupBoxPicture, 0, 1, 2, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        TSSClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(TSSClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 872, 26));
        menuMenu = new QMenu(menuBar);
        menuMenu->setObjectName(QString::fromUtf8("menuMenu"));
        menuFilters = new QMenu(menuBar);
        menuFilters->setObjectName(QString::fromUtf8("menuFilters"));
        menuDither_random = new QMenu(menuFilters);
        menuDither_random->setObjectName(QString::fromUtf8("menuDither_random"));
        TSSClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(TSSClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        TSSClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(TSSClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        TSSClass->setStatusBar(statusBar);

        menuBar->addAction(menuMenu->menuAction());
        menuBar->addAction(menuFilters->menuAction());
        menuMenu->addAction(actionOpen);
        menuMenu->addAction(actionClose);
        menuMenu->addAction(actionExit);
        menuFilters->addAction(actionDitherFloydSteinberg);
        menuFilters->addAction(menuDither_random->menuAction());
        menuFilters->addAction(actionOld_film);
        menuDither_random->addAction(actionDitherRed);
        menuDither_random->addAction(actionDitherBlue);
        menuDither_random->addAction(actionDitherGreen);
        menuDither_random->addAction(actionDitherCyan);
        menuDither_random->addAction(actionDitherMagenta);
        menuDither_random->addAction(actionDitherYellow);
        menuDither_random->addAction(actionDitherBlackWhite);

        retranslateUi(TSSClass);
        QObject::connect(actionOpen, SIGNAL(triggered()), TSSClass, SLOT(actionOpen()));
        QObject::connect(actionClose, SIGNAL(triggered()), TSSClass, SLOT(actionClose()));
        QObject::connect(actionExit, SIGNAL(triggered()), TSSClass, SLOT(actionExit()));
        QObject::connect(listWidget, SIGNAL(currentRowChanged(int)), TSSClass, SLOT(listWidgetItemChanged(int)));
        QObject::connect(RedCheckBox, SIGNAL(stateChanged(int)), TSSClass, SLOT(redChanged(int)));
        QObject::connect(GreenCheckBox, SIGNAL(stateChanged(int)), TSSClass, SLOT(greenChanged(int)));
        QObject::connect(BlueCheckBox, SIGNAL(stateChanged(int)), TSSClass, SLOT(blueChanged(int)));
        QObject::connect(LockBitsCheckBox, SIGNAL(stateChanged(int)), TSSClass, SLOT(lockBitsChanged(int)));
        QObject::connect(CurveDrawingCheckBox, SIGNAL(stateChanged(int)), TSSClass, SLOT(curveDrawingChanged(int)));
        QObject::connect(actionDitherFloydSteinberg, SIGNAL(triggered()), TSSClass, SLOT(ditherFloydSteinbergClicked()));
        QObject::connect(actionDitherBlackWhite, SIGNAL(triggered()), TSSClass, SLOT(ditherBlackWhiteClicked()));
        QObject::connect(actionOld_film, SIGNAL(triggered()), TSSClass, SLOT(oldFilmClicked()));
        QObject::connect(actionDitherBlue, SIGNAL(triggered()), TSSClass, SLOT(ditherBlueClicked()));
        QObject::connect(actionDitherCyan, SIGNAL(triggered()), TSSClass, SLOT(ditherCyanClicked()));
        QObject::connect(actionDitherGreen, SIGNAL(triggered()), TSSClass, SLOT(ditherGreenClicked()));
        QObject::connect(actionDitherMagenta, SIGNAL(triggered()), TSSClass, SLOT(ditherMagentaClicked()));
        QObject::connect(actionDitherRed, SIGNAL(triggered()), TSSClass, SLOT(ditherRedClicked()));
        QObject::connect(actionDitherYellow, SIGNAL(triggered()), TSSClass, SLOT(ditherYellowClicked()));

        QMetaObject::connectSlotsByName(TSSClass);
    } // setupUi

    void retranslateUi(QMainWindow *TSSClass)
    {
        TSSClass->setWindowTitle(QApplication::translate("TSSClass", "TSS", nullptr));
        actionOpen->setText(QApplication::translate("TSSClass", "Open", nullptr));
        actionClose->setText(QApplication::translate("TSSClass", "Close", nullptr));
        actionExit->setText(QApplication::translate("TSSClass", "Exit", nullptr));
        actionDitherFloydSteinberg->setText(QApplication::translate("TSSClass", "Dither - Floyd Steinberg", nullptr));
        actionOld_film->setText(QApplication::translate("TSSClass", "Old film", nullptr));
        actionDitherRed->setText(QApplication::translate("TSSClass", "Red", nullptr));
        actionDitherBlue->setText(QApplication::translate("TSSClass", "Green", nullptr));
        actionDitherGreen->setText(QApplication::translate("TSSClass", "Blue", nullptr));
        actionDitherCyan->setText(QApplication::translate("TSSClass", "Cyan", nullptr));
        actionDitherMagenta->setText(QApplication::translate("TSSClass", "Magenta", nullptr));
        actionDitherYellow->setText(QApplication::translate("TSSClass", "Yellow", nullptr));
        actionDitherBlackWhite->setText(QApplication::translate("TSSClass", "Black-white", nullptr));
        groupBoxFileTree->setTitle(QApplication::translate("TSSClass", "File tree", nullptr));
        groupBoxHistogram->setTitle(QApplication::translate("TSSClass", "Histogram", nullptr));
        BlueCheckBox->setText(QApplication::translate("TSSClass", "Blue", nullptr));
        GreenCheckBox->setText(QApplication::translate("TSSClass", "Green", nullptr));
        CurveDrawingCheckBox->setText(QApplication::translate("TSSClass", "Curve drawing", nullptr));
        LockBitsCheckBox->setText(QApplication::translate("TSSClass", "Lock bits", nullptr));
        RedCheckBox->setText(QApplication::translate("TSSClass", "Red", nullptr));
        groupBoxPicture->setTitle(QApplication::translate("TSSClass", "Picture preview", nullptr));
        menuMenu->setTitle(QApplication::translate("TSSClass", "File", nullptr));
        menuFilters->setTitle(QApplication::translate("TSSClass", "Filters", nullptr));
        menuDither_random->setTitle(QApplication::translate("TSSClass", "Dither - random", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TSSClass: public Ui_TSSClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TSS_H
