#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QList>
#include <QWidget>
#include <QtWidgets>
#include <cmath>
#include <iostream>

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	bool newImage(int x, int y);
	bool openImage(const QString &fileName);
	QSize scaleImage(int width, int height, int newWidth, int newHeight);
	bool saveImage(const QString &fileName);
	int getHeight();
	int getWidth();
	QImage getImage();
	const uchar *getImageBits();
	int getByteCount();
	void setPainterWidgetSize(int x, int y);
	bool isModified() const { return modified; }
	void changeImage(QImage newImage);

public slots:
	void clearImage();

protected:
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

private:

	int painterWidth;
	int painterHeight;
	bool modified;
	bool painting;
	QImage image;
};