#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_TSS.h"
#include "PaintWidget.h"
#include "Histogram.h"
#include "Filters.h"
#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QRect>
#include <QString>
#include <QFileDialog>
#include <QMessageBox>
#include <QThread>
#include <thread>

class FilterThread : public QObject
{
	Q_OBJECT
public:
	QThread* thread;
	FilterThread(QObject* parent = Q_NULLPTR);
	~FilterThread();
};

class TSS : public QMainWindow
{
	Q_OBJECT

public:
	TSS(QWidget *parent = Q_NULLPTR);

public slots:
	void actionOpen();
	void actionClose();
	void actionExit();
	void ditherFloydSteinbergClicked();
	void ditherRedClicked();
	void ditherGreenClicked();
	void ditherBlueClicked();
	void ditherCyanClicked();
	void ditherMagentaClicked();
	void ditherYellowClicked();
	void ditherBlackWhiteClicked();
	void oldFilmClicked();
	void listWidgetItemChanged(int index);
	void redChanged(int state);
	void greenChanged(int state);
	void blueChanged(int state);
	void lockBitsChanged(int state);
	void curveDrawingChanged(int state);
	void startDitherFSThread();
	void startDitherBasicThread(QString color);
	void startOldFilm();
	void actionsAfterCompletion();
	void displayDitherFS();
	void displayDitherBasic();
	void displayOldFilm();
	void interruptThread();

signals:
	void launchComputation();

private:
	void drawHistogram();
	Ui::TSSClass ui;
	PaintWidget paintWidget;
	Histogram histogram;
	Filters* activeFilter = NULL;
	FilterThread* processThread = NULL;
	bool filterComputationStarted = false;

};
/*
Pozri si v ImageViewer.cpp proceduru void

startBlurComputationThread() alebo
startSharpenComputationThread()

tam sa vytvori thread ako objekt FilterThread(QObject* parent)
potom tam este treba spravit zopar veci ako movenut objekt(konkretne ImageFilter) do tohoto threadu a spustit

_activeFilter->moveToThread(_process_thread->thread);
_process_thread->thread->start();

potom treba vytvorit callbacky :

connect(this, SIGNAL(launchComputation()), _activeFilter, SLOT(applyBlur()));
connect(_activeFilter, SIGNAL(filterComputationComplete()), this, SLOT(actionsAfterCompletion()));

ktore napr.konkretne spoja signal launchComputation() s metodou filtra applyBlur() a potom
ked filter dopocita potrebuje okienkova aplikacia vediet, ze vypocet skoncil, teda prejde signal
filterComputationComplete() do actionsAfterCompletion().V metode, ktora ma emitovat signal, aby dostal odozvu,
treba spravit este emit filterComputationComplete(); alebo emit progressIncremented(); (to je zas iny thread pre progress bar)
*/

