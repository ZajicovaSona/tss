#pragma once

#include <QVector3D>
#include <QRgb>
#include <QColor>
#include <QImage>
#include <QMessageBox>
#include <thread>
#include <cmath>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/mean.hpp>

class Filters: public QObject
{
	Q_OBJECT
public:
	Filters(QString type, QImage original,QString color);
	~Filters();
	void createPalette(qint32 paletteSize);
	void clearPalette();
	
	QString getType();
	QImage getOriginalImage();
	QImage getDitherFSImage();
	QImage getDitherBasicImage();
	QImage getOldFilmImage();
	QImage ditherBlackWhite();

public slots:
	void colorReduce();
	void ditherBasic();
	void ditherFloydSteinberg();

signals:
	void filterComputationComplete();

private:
	typedef QVector<QRgb> QColorVector;
	QVector<QColor> palette;
	
	QColor colorFromPalette(QRgb color);
	void splitBin(QColorVector::Iterator start, QColorVector::Iterator end, qint32 remaining, const qint32 bins);
	QImage imageDitherFS;
	QImage imageDitherBasic;
	QImage imageOldFilm;
	QImage originalImage;
	QString filter;
	QString ditherColor;

};

