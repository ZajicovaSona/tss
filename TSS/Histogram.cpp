#include "histogram.h"
struct imageBits {
	const uchar *image;
	int size;
};

Histogram::Histogram(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	R.resize(256);
	G.resize(256);
	B.resize(256);
	clearHistogram();
}

bool Histogram::newImage(int x, int y)
{
	QImage loadedImage(x, y, QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

int Histogram::getHeight()
{
	return image.height();
}

int Histogram::getWidth()
{
	return image.width();
}

void Histogram::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void Histogram::calculateHistogram(QImage &loadedImage)
{
	for (int i = 0; i < loadedImage.width(); i++) {
		for (int j = 0; j < loadedImage.height(); j++) {
			QColor color = loadedImage.pixelColor(i,j);
			R[color.red()]++;
			G[color.green()]++;
			B[color.blue()]++;
		}
	}
}

void Histogram::calculateHistogramBits(QByteArray &loadedImage)
{
	QMessageBox mbox;
	int r, g, b;
	for (int i = 0; i < (loadedImage.size()); i = i + 4) {
		b = loadedImage[i];
		g = loadedImage[i + 1];
		r = loadedImage[i + 2];
		if (b < 0) {
			b = 256-abs(loadedImage[i]);
		}
		if (g < 0) {
			g = 256-abs(loadedImage[i + 1]);
		}
		if (r < 0) {
			r = 256-abs(loadedImage[i + 2]);
		}
		B[b]++;
		G[g]++;
		R[r]++;
	}
	mbox.setText("Lock bits");
	//mbox.exec();
}

void Histogram::clearHistogram()
{
	for (int i = 0; i < 256; i++) {
		R[i] = 0;
		G[i] = 0;
		B[i] = 0;
	}
	image.fill(qRgb(255, 255, 255));
	update();
}

void Histogram::drawHistogram(int colour)
{
	int scaledValue;
	if (colour == 0) {			//red
		int max_red = 0;
		for (int i = 0; i < 256; i++)	//find max Red
		{
			if (R[i] > max_red)
				max_red = R[i];
		}
		for (int i = 0; i < image.width(); i++) {
			scaledValue = round( R[i] * ((float)image.height()/max_red) );
			for (int j = 0; j < scaledValue; j++)
				image.setPixelColor(i, image.height()- j, Qt::red);
		}
	}
	else if (colour == 1) {		//green
		int max_green = 0;
		for (int i = 0; i < 256; i++)	//find max green
		{
			if (G[i] > max_green)
				max_green = G[i];
		}
		for (int i = 0; i < image.width(); i++) {
			scaledValue = round(G[i] * ((float)image.height() / max_green));
			for (int j = 0; j < scaledValue; j++)
				image.setPixelColor(i, image.height() - j, Qt::green);
		}
	}
	else {						//blue
		int max_blue = 0;
		for (int i = 0; i < 256; i++)	//find max Blue
		{
			if (B[i] > max_blue)
				max_blue = B[i];
		}
		for (int i = 0; i < image.width(); i++) {
			scaledValue = round(B[i] * ((float)image.height() / max_blue));
			for (int j = 0; j < scaledValue; j++)
				image.setPixelColor(i, image.height() - 1 - j, Qt::blue);
		}
	}
	update();
}

void Histogram::curveDrawing(int colour)
{
	int scaledValue;
	if (colour == 0) {			//red
		int max_red = 0;
		int tmp = 0;
		for (int i = 0; i < 256; i++)	//find max Red
		{
			if (R[i] > max_red)
				max_red = R[i];
		}
		for (int i = 0; i < image.width(); i++) {
			scaledValue = round(R[i] * ((float)image.height() / max_red));
			if (tmp == 0)
				image.setPixelColor(i, image.height() - scaledValue, Qt::red);
			else if (tmp < scaledValue)
				for (int j = tmp; j <= scaledValue;j++)
					image.setPixelColor(i, image.height() - j, Qt::red);
			else
				for (int j = scaledValue; j <= tmp; j++)
					image.setPixelColor(i, image.height() - j, Qt::red);
			tmp = scaledValue;
		}
	}
	else if (colour == 1) {		//green
		int max_green = 0;
		int tmp = 0;
		for (int i = 0; i < 256; i++)	//find max green
		{
			if (G[i] > max_green)
				max_green = G[i];
		}
		for (int i = 0; i < image.width(); i++) {
			scaledValue = round(G[i] * ((float)image.height() / max_green));
			if (tmp == 0)
				image.setPixelColor(i, image.height() - scaledValue, Qt::green);
			else if (tmp < scaledValue)
				for (int j = tmp; j <= scaledValue; j++)
					image.setPixelColor(i, image.height() - j, Qt::green);
			else
				for (int j = scaledValue; j <= tmp; j++)
					image.setPixelColor(i, image.height() - j, Qt::green);
			tmp = scaledValue;
		}
	}
	else {						//blue
		int max_blue = 0;
		int tmp = 0;
		for (int i = 0; i < 256; i++)	//find max Blue
		{
			if (B[i] > max_blue)
				max_blue = B[i];
		}
		for (int i = 0; i < image.width(); i++) {
			scaledValue = round(B[i] * ((float)image.height() / max_blue));
			if (tmp == 0)
				image.setPixelColor(i, image.height() - scaledValue, Qt::blue);
			else if (tmp < scaledValue)
				for (int j = tmp; j <= scaledValue; j++)
					image.setPixelColor(i, image.height() - j, Qt::blue);
			else
				for (int j = scaledValue; j <= tmp; j++)
					image.setPixelColor(i, image.height() - j, Qt::blue);
			tmp = scaledValue;
		}
	}
	update();
}

void Histogram::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void Histogram::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void Histogram::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void Histogram::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}