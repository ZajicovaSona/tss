#include "TSS.h"

TSS::TSS(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.gridLayoutPicture->addWidget(&this->paintWidget, 0, 0);
	ui.gridLayoutHistogram->addWidget(&this->histogram, 0, 1, 8, 3);
	paintWidget.newImage(850, 850);
	paintWidget.setPainterWidgetSize(850, 850);
	histogram.newImage(256, 300);
	//vlozit pocitanie filtrov do samostatnych threadov
	//calculate histogram az ked sa prvy krat zaklikne R, G, alebo B
	//calculateHistogramBits dat do samostatneho threadu
	//schovat vsetko do struktury calc - bitmapy, vektory R,G,B
}

void TSS::actionOpen() {
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty()) {
		ui.listWidget->addItem(fileName);
		ui.listWidget->setCurrentRow(ui.listWidget->count() - 1);
	}
}

void TSS::actionClose() {
	ui.listWidget->takeItem(ui.listWidget->currentRow());

	if (ui.listWidget->count() > 0) 
		ui.listWidget->setCurrentRow(0);
}

void TSS::actionExit() {

}

void TSS::ditherFloydSteinbergClicked()
{	
	filterComputationStarted = true;
	startDitherFSThread();
}

void TSS::ditherRedClicked()
{
	filterComputationStarted = true;
	startDitherBasicThread("red");
}

void TSS::ditherGreenClicked()
{
	filterComputationStarted = true;
	startDitherBasicThread("green");
}

void TSS::ditherBlueClicked()
{
	filterComputationStarted = true;
	startDitherBasicThread("blue");
}

void TSS::ditherCyanClicked()
{
	filterComputationStarted = true;
	startDitherBasicThread("cyan");
}

void TSS::ditherMagentaClicked()
{
	filterComputationStarted = true;
	startDitherBasicThread("magenta");
}

void TSS::ditherYellowClicked()
{
	filterComputationStarted = true;
	startDitherBasicThread("yellow");
}

void TSS::ditherBlackWhiteClicked()
{
	QImage displayedImage = activeFilter->ditherBlackWhite();
	paintWidget.changeImage(displayedImage);
	QCoreApplication::processEvents();
	histogram.clearImage();
	histogram.calculateHistogram(displayedImage);
	drawHistogram();
}

void TSS::oldFilmClicked()
{
	filterComputationStarted = true;
	startOldFilm();
}

void TSS::listWidgetItemChanged(int index)
{	
	histogram.clearHistogram();
	
	if (ui.listWidget->item(index) == NULL) {
		paintWidget.clearImage();
		return;
	}

	QString fileName = ui.listWidget->item(index)->text();
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
	
	if (ui.LockBitsCheckBox->isChecked()) {
		QByteArray image((char *)paintWidget.getImageBits(), paintWidget.getByteCount());
		histogram.calculateHistogramBits(image);
	}
	else
		histogram.calculateHistogram(paintWidget.getImage());

	drawHistogram();
}

void TSS::redChanged(int state)
{
	QMessageBox mbox;
	
	histogram.clearImage();
	drawHistogram();
}

void TSS::greenChanged(int state)
{
	histogram.clearImage();
	drawHistogram();
}

void TSS::blueChanged(int state)
{
	histogram.clearImage();
	drawHistogram();
}

void TSS::lockBitsChanged(int state)
{
	histogram.clearHistogram();

	if (ui.LockBitsCheckBox->isChecked()) {
		QByteArray image((char *)paintWidget.getImageBits(), paintWidget.getByteCount());
		histogram.calculateHistogramBits(image);
	}
	else
		histogram.calculateHistogram(paintWidget.getImage());
	
	drawHistogram();
}

void TSS::curveDrawingChanged(int state)
{
	histogram.clearImage();
	drawHistogram();
}

void TSS::startDitherFSThread()
{
	processThread = new FilterThread(this);
	activeFilter = new Filters("ditherFS",paintWidget.getImage(),"black");
	activeFilter->createPalette(60);

	activeFilter->moveToThread(processThread->thread);
	processThread->thread->start();

	connect(this, &TSS::launchComputation, activeFilter, &Filters::ditherFloydSteinberg);
	connect(activeFilter, &Filters::filterComputationComplete, this, &TSS::actionsAfterCompletion);

	emit launchComputation();
}

void TSS::startDitherBasicThread(QString color)
{
	processThread = new FilterThread(this);
	activeFilter = new Filters("ditherBasic", paintWidget.getImage(),color);

	activeFilter->moveToThread(processThread->thread);
	processThread->thread->start();

	connect(this, &TSS::launchComputation, activeFilter, &Filters::ditherBasic);
	connect(activeFilter, &Filters::filterComputationComplete, this, &TSS::actionsAfterCompletion);

	emit launchComputation();
}

void TSS::startOldFilm()
{
	processThread = new FilterThread(this);
	activeFilter = new Filters("oldFilm", paintWidget.getImage(),"black");
	activeFilter->createPalette(60);
	
	activeFilter->moveToThread(processThread->thread);
	processThread->thread->start();

	connect(this, &TSS::launchComputation, activeFilter, &Filters::colorReduce);
	connect(activeFilter, &Filters::filterComputationComplete, this, &TSS::actionsAfterCompletion);

	emit launchComputation();
}

void TSS::actionsAfterCompletion()
{
	filterComputationStarted = false;
	activeFilter->clearPalette();

	if (activeFilter->getType() == "ditherFS") {
		displayDitherFS();
	}
	else if (activeFilter->getType() == "ditherBasic") {
		displayDitherBasic();
	}
	else if (activeFilter->getType() == "oldFilm") {
		displayOldFilm();
	}
	processThread->thread->exit(1);
	
	
}

void TSS::displayDitherFS()
{
	if (activeFilter != NULL && !filterComputationStarted) {
		QImage displayedImage;
		
		displayedImage = activeFilter->getDitherFSImage();
		if (!displayedImage.isNull()) {
			paintWidget.changeImage(displayedImage);
			QCoreApplication::processEvents();
			histogram.clearImage();
			histogram.calculateHistogram(displayedImage);
			drawHistogram();
		}
	}
}

void TSS::displayDitherBasic()
{
	if (activeFilter != NULL && !filterComputationStarted) {
		QImage displayedImage;

		displayedImage = activeFilter->getDitherBasicImage();
		if (!displayedImage.isNull()) {
			paintWidget.changeImage(displayedImage);
			QCoreApplication::processEvents();
			histogram.clearImage();
			histogram.calculateHistogram(displayedImage);
			drawHistogram();
		}
	}
}

void TSS::displayOldFilm()
{
	if (activeFilter != NULL && !filterComputationStarted) {
		QImage displayedImage;

		displayedImage = activeFilter->getOldFilmImage();
		if (!displayedImage.isNull()) {
			paintWidget.changeImage(displayedImage);
			QCoreApplication::processEvents();
			histogram.clearImage();
			histogram.calculateHistogram(displayedImage);
			drawHistogram();
		}
	}
}

void TSS::interruptThread()
{
	processThread->thread->terminate();
}

void TSS::drawHistogram()
{
	if (ui.CurveDrawingCheckBox->isChecked()) {
		if (ui.RedCheckBox->isChecked())
			histogram.curveDrawing(0);
		if (ui.GreenCheckBox->isChecked())
			histogram.curveDrawing(1);
		if (ui.BlueCheckBox->isChecked())
			histogram.curveDrawing(2);
	}
	else {
		if (ui.RedCheckBox->isChecked())
			histogram.drawHistogram(0);
		if (ui.GreenCheckBox->isChecked())
			histogram.drawHistogram(1);
		if (ui.BlueCheckBox->isChecked())
			histogram.drawHistogram(2);
	}
}

FilterThread::FilterThread(QObject* parent)
{
	thread = new QThread(parent);
}

FilterThread::~FilterThread()
{
	if (thread->isRunning()) {
		thread->terminate();
	}
}
