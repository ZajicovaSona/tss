#define HISTOGRAM_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QList>
#include <QVector>
#include <QWidget>
#include <QtWidgets>
#include <cmath>
#include <iostream>

class Histogram : public QWidget
{
	Q_OBJECT
public:
	Histogram(QWidget *parent = 0);
	void clearImage();
	bool newImage(int x, int y);
	int getHeight();
	int getWidth();
	void calculateHistogram(QImage &loadedImage);
	void calculateHistogramBits(QByteArray &loadedImage);
	void clearHistogram();
	void drawHistogram(int colour);
	void curveDrawing(int colour);

	bool isModified() const { return modified; }

protected:
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);

	bool modified;
	bool painting;
	int myPenWidth;
	QVector<int> R, G, B;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
};

